# Changelog

## Unreleased

[View Commits](https://gitlab.com/nucleardog/php-telemetry/-/compare/v0.0.10...master)

## v0.0.10 (2024-03-29)

[View Commits](https://gitlab.com/nucleardog/php-telemetry/-/compare/v0.0.9...v0.0.10)

* Clean up scope handling. Hopefully.
* `Telemetry::shutdown()` now attempts to close all open spans/scopes at shutdown.
* Document most of the useful methods on the Trace facade for Laravel.

## v0.0.9 (2024-03-25)

[View Commits](https://gitlab.com/nucleardog/php-telemetry/-/compare/v0.0.8...v0.0.9)

* Provide manual shutdown() method.

## v0.0.8 (2024-03-21)

[View Commits](https://gitlab.com/nucleardog/php-telemetry/-/compare/v0.0.7...v0.0.8)

* Instrument outgoing http requests
* Creates spans covering outgoing HTTP requests
* Attaches trace id to outgoing requests to facilitate distributed tracing

## v0.0.7 (2024-03-21)

[View Commits](https://gitlab.com/nucleardog/php-telemetry/-/compare/v0.0.6...v0.0.7)

* Bugfix

## v0.0.6 (2024-03-21)

[View Commits](https://gitlab.com/nucleardog/php-telemetry/-/compare/v0.0.5...v0.0.6)

* Instrument processes
* Standardize span attributes

## v0.0.5 (2024-03-21)

[View Commits](https://gitlab.com/nucleardog/php-telemetry/-/compare/v0.0.4...v0.0.5)

* Bugfix: Okay, maybe now it doesn't.

## v0.0.4 (2024-03-21)

[View Commits](https://gitlab.com/nucleardog/php-telemetry/-/compare/v0.0.3...v0.0.4)

* Bugfix: Passing attributes when entering span no longer calls non-existent method.

## v0.0.3 (2024-03-21)

[View Commits](https://gitlab.com/nucleardog/php-telemetry/-/compare/v0.0.2...v0.0.3)

* `in()` helper will now set thrown exceptions on the span.

## v0.0.2 (2024-03-21)

[View Commits](https://gitlab.com/nucleardog/php-telemetry/-/compare/v0.0.1...v0.0.2)

* Now add a "type" attribute to spans generated from the Laravel Requests/Jobs
  auto-instrumentation.
* Added an `in()` helper to Contexts for simplifying running a closure in a
  context.

## v0.0.1 (2024-03-20)

[View Commits](https://gitlab.com/nucleardog/php-telemetry/-/commits/v0.0.1)

* Initial commit.