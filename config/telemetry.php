<?php

return [

	'app' => [
		'name' => env('APP_NAME', 'Laravel'),
		// 'namespace' => '',
		'environment' => env('APP_ENV', 'production'),
		// 'version' => '',
	],

	'options' => [
		'is_gcp' => env('TELEMETRY_OPTIONS_IS_GCP', false),
	],

	'endpoint' => [
		// null, console, otlp-http, otlp-grpc
		'type' => env('TELEMETRY_ENDPOINT_TYPE', 'otlp-http'),
		// For otlp-http and otlp-grpc
		'url' => env('TELEMETRY_ENDPOINT_URL', 'http://127.0.0.1:4318'),
	],

	'instrument' => [
		'jobs' => env('TELEMETRY_INSTRUMENT_JOBS', true),
		'requests' => env('TELEMETRY_INSTRUMENT_REQUESTS', true),
		'exceptions' => env('TELEMETRY_INSTRUMENT_EXCEPTIONS', true),
		'logs' => env('TELEMETRY_INSTRUMENT_LOGS', true),
		'processes' => env('TELEMETRY_INSTRUMENT_PROCESSES', true),
		'http_client' => env('TELEMETRY_INSTRUMENT_HTTP_CLIENT', true),
	],

	'requests' => [
		// If the X-Trace header is present in the request, we will use that
		// as the parent trace and span for our request.
		//'continue_traces' => true,
	],

];
