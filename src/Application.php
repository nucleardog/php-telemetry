<?php

declare(strict_types=1);

namespace Nucleardog\Telemetry;

use OpenTelemetry\SDK\Resource\ResourceInfo;
use OpenTelemetry\SDK\Common\Attribute\Attributes;
use OpenTelemetry\SemConv\ResourceAttributes;

class Application
{
	public function __construct(
		public readonly string $name,
		public readonly ?string $namespace = null,
		public readonly ?string $environment = null,
		public readonly ?string $version = null,
	) {
	}

	public function build(): ResourceInfo
	{
		$attributes = [
			ResourceAttributes::SERVICE_NAME => $this->name,
		];

		if ($this->namespace !== null)
		{
			$attributes[ResourceAttributes::SERVICE_NAMESPACE] = $this->namespace;
		}

		if ($this->environment !== null)
		{
			$attributes[ResourceAttributes::DEPLOYMENT_ENVIRONMENT] = $this->environment;
		}

		if ($this->version !== null)
		{
			$attributes[ResourceAttributes::SERVICE_VERSION] =  $this->version;
		}

		return ResourceInfo::create(Attributes::create($attributes));
	}


}