<?php

declare(strict_types=1);

namespace Nucleardog\Telemetry;

abstract class Attributes
{
	private static object $missingValue;

	public static function get(string $key): string
	{
		if (config('telemetry.options.is_gcp'))
			$cases = Enums\AttributesGcp::cases();
		else
			$cases = Enums\Attributes::cases();

		foreach ($cases as $enum)
		{
			if ($enum->name === $key)
				return $enum->value;
		}

		throw new \DomainException(sprintf('Unknown context attribute: %s', $key));
	}

	public static function resolve(array $attributes): array
	{
		$return = [];

		foreach ($attributes as $key=>$value)
		{
			static::set($return, $key, $value);
		}

		return $return;
	}

	public static function set(&$target, string $key, $value): void
	{
		if (static::isMissingValue($value))
			return;

		if ($value instanceof \Closure)
			$value = $value();

		if ($value instanceof \BackedEnum)
			$value = $value->value;

		if ($key instanceof \UnitEnum)
			$key = $key->name;

		$attribute = static::get($key);
		$target[$attribute] = $value;
	}

	public static function fill(&$target, array $attributes): void
	{
		foreach ($attributes as $k=>$v)
		{
			static::set($target, $k, $v);
		}
	}

	public static function when(bool|Closure $condition, $value)
	{
		if (!is_bool($condition))
			$condition = $condition();

		if (!$condition)
			return static::missingValue();

		if ($value instanceof \Closure)
			$value = $value();

		return $value;
	}

	private static function missingValue(): object
	{
		if (!isset(static::$missingValue))
			static::$missingValue = (object)[];
		return static::$missingValue;
	}

	private static function isMissingValue($thing): bool
	{
		return isset(static::$missingValue) && $thing === static::$missingValue;
	}

}