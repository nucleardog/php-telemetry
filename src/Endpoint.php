<?php

declare(strict_types=1);

namespace Nucleardog\Telemetry;
use Closure;
use OpenTelemetry\API\Trace\TracerInterface;
use OpenTelemetry\API\Logs\LoggerInterface;
use OpenTelemetry\API\Metrics\MeterInterface;

use OpenTelemetry\SDK\Logs\LoggerProvider;
use OpenTelemetry\SDK\Trace\TracerProvider;
use OpenTelemetry\SDK\Metrics\MeterProvider;

abstract class Endpoint
{
	private array $providers = [];

	public abstract function getLogger(Application $app): LoggerInterface;
	public abstract function getTracer(Application $app): TracerInterface;
	public abstract function getMetrics(Application $app): MeterInterface;

	public function shutdown(): void
	{
		foreach ($this->providers as $provider)
		{
			if (method_exists($provider, 'shutdown'))
			{
				$provider->shutdown();
			}
		}
		$this->providers = [];
	}

	protected function buildLoggerProvider(Closure $callback): LoggerProvider
	{
		return $this->buildProvider(LoggerProvider::class, $callback);
	}

	protected function buildTracerProvider(Closure $callback): TracerProvider
	{
		return $this->buildProvider(TracerProvider::class, $callback);
	}

	protected function buildMeterProvider(Closure $callback): MeterProvider
	{
		return $this->buildProvider(MeterProvider::class, $callback);
	}

	private function buildProvider(string $class, Closure $callback)
	{
		$builder = $class::builder();
		$callback($builder);
		$provider = $builder->build();
		$this->providers[] = $provider;
		return $provider;
	}

}