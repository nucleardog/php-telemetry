<?php

declare(strict_types=1);

namespace Nucleardog\Telemetry\Endpoints;

use OpenTelemetry\API\Trace\TracerInterface;
use OpenTelemetry\API\Logs\LoggerInterface;
use OpenTelemetry\API\Metrics\MeterInterface;

use OpenTelemetry\SDK\Common\Export\TransportInterface;
use OpenTelemetry\SDK\Common\Export\Stream\StreamTransport;

use OpenTelemetry\SDK\Logs\LoggerProvider;
use OpenTelemetry\SDK\Logs\Exporter\ConsoleExporter;
use OpenTelemetry\SDK\Logs\Processor\SimpleLogRecordProcessor;

use OpenTelemetry\SDK\Trace\TracerProvider;
use OpenTelemetry\SDK\Trace\SpanProcessor\SimpleSpanProcessor;
use OpenTelemetry\SDK\Trace\SpanExporter\ConsoleSpanExporter;

use OpenTelemetry\SDK\Metrics\MeterProvider;
use OpenTelemetry\SDK\Metrics\MetricReader\ExportingReader;
use OpenTelemetry\SDK\Metrics\MetricExporter\ConsoleMetricExporter;

use Nucleardog\Telemetry\Application;
use Nucleardog\Telemetry\Endpoint;

class ConsoleEndpoint extends Endpoint
{
	private function getTransport(): TransportInterface
	{
		static $transport;
		return $transport ??= new StreamTransport(fopen('php://stdout', 'w'), 'application/x-ndjson');
	}

	public function getLogger(Application $app): LoggerInterface
	{
		return $this->buildLoggerProvider(
			fn($builder) => $builder
				->setResource($app->build())
				->addLogRecordProcessor(new SimpleLogRecordProcessor(new ConsoleExporter($this->getTransport())))
		)->getLogger($app->name);
		// TODO: What does the name really do for us here?
		//       Is this supposed to be the request url/etc like the traces?
	}

	public function getTracer(Application $app): TracerInterface
	{
		return $this->buildTracerProvider(
			fn($builder) => $builder
				->setResource($app->build())
				->addSpanProcessor(new SimpleSpanProcessor(new ConsoleSpanExporter($this->getTransport())))
		)->getTracer($app->name);
	}

	public function getMetrics(Application $app): MeterInterface
	{
		return $this->buildMeterProvider(
			fn($builder) => $builder
				->setResource($app->build())
				->addReader(new ExportingReader(new ConsoleMetricExporter()))
		)->getMeter($app->name);
	}

}