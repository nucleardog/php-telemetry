<?php

declare(strict_types=1);

namespace Nucleardog\Telemetry\Endpoints;

use OpenTelemetry\API\Trace\TracerInterface;
use OpenTelemetry\API\Logs\LoggerInterface;
use OpenTelemetry\API\Metrics\MeterInterface;

use OpenTelemetry\API\Logs\NoopLogger;
use OpenTelemetry\API\Metrics\Noop\NoopMeter;
use OpenTelemetry\API\Trace\NoopTracer;

use Nucleardog\Telemetry\Application;
use Nucleardog\Telemetry\Endpoint;

class NullEndpoint extends Endpoint
{

	public function getLogger(Application $app): LoggerInterface
	{
		return new NoopLogger();
	}

	public function getTracer(Application $app): TracerInterface
	{
		return new NoopTracer();
	}

	public function getMetrics(Application $app): MeterInterface
	{
		return new NoopMeter();

	}

}