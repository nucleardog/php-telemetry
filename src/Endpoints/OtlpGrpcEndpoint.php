<?php

declare(strict_types=1);

namespace Nucleardog\Telemetry\Endpoints;

use OpenTelemetry\API\Trace\TracerInterface;
use OpenTelemetry\API\Logs\LoggerInterface;
use OpenTelemetry\API\Metrics\MeterInterface;

use OpenTelemetry\SDK\Common\Export\TransportInterface;
use OpenTelemetry\Contrib\Grpc\GrpcTransportFactory;
use OpenTelemetry\Contrib\Otlp\OtlpUtil;
use OpenTelemetry\Contrib\Otlp\LogsExporter;
use OpenTelemetry\Contrib\Otlp\SpanExporter;
use OpenTelemetry\Contrib\Otlp\MetricExporter;
use OpenTelemetry\API\Signals;
use OpenTelemetry\SDK\Trace\SpanProcessor\SimpleSpanProcessor;
use OpenTelemetry\SDK\Logs\Processor\BatchLogRecordProcessor;
use OpenTelemetry\SDK\Metrics\MetricReader\ExportingReader;
use OpenTelemetry\SDK\Common\Time\ClockFactory;

use Nucleardog\Telemetry\Application;
use Nucleardog\Telemetry\Endpoint;

class OtlpGrpcEndpoint extends Endpoint
{

	public function __construct(
		private string $url,
		private string $service,
	) {
		$this->url = rtrim($this->url, '/');
	}

	private function getTransport(string $signal): TransportInterface
	{
		$factory = new GrpcTransportFactory();
		return $factory->create(sprintf('%s%s', $this->url, OtlpUtil::method($signal)));
	}

	public function getLogger(Application $app): LoggerInterface
	{
		return $this->buildLoggerProvider(
			fn($builder) => $builder
				->setResource($app->build())
				->addLogRecordProcessor(new BatchLogRecordProcessor(new LogsExporter($this->getTransport(Signals::LOGS)), ClockFactory::getDefault()))
		)->getLogger($app->name);
	}

	public function getTracer(Application $app): TracerInterface
	{
		return $this->buildTracerProvider(
			fn($builder) => $builder
				->setResource($app->build())
				->addSpanProcessor(new SimpleSpanProcessor(new SpanExporter($this->getTransport(Signals::TRACE))))
		)->getTracer($app->name);
	}

	public function getMetrics(Application $app): MeterInterface
	{
		return $this->buildMeterProvider(
			fn($builder) => $builder
				->setResource($app->build())
				->addReader(new ExportingReader(new MetricExporter($this->getTransport(Signals::METRICS))))
		)->getMeter($app->name);
	}

}