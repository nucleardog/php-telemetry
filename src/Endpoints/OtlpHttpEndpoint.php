<?php

declare(strict_types=1);

namespace Nucleardog\Telemetry\Endpoints;

use OpenTelemetry\API\Trace\TracerInterface;
use OpenTelemetry\API\Logs\LoggerInterface;
use OpenTelemetry\API\Metrics\MeterInterface;

use OpenTelemetry\SDK\Common\Export\TransportInterface;
use OpenTelemetry\Contrib\Otlp\OtlpHttpTransportFactory;
use OpenTelemetry\Contrib\Otlp\ContentTypes;
use OpenTelemetry\Contrib\Otlp\LogsExporter;
use OpenTelemetry\Contrib\Otlp\SpanExporter;
use OpenTelemetry\Contrib\Otlp\MetricExporter;
use OpenTelemetry\SDK\Trace\SpanProcessor\SimpleSpanProcessor;
use OpenTelemetry\SDK\Logs\Processor\BatchLogRecordProcessor;
use OpenTelemetry\SDK\Metrics\MetricReader\ExportingReader;
use OpenTelemetry\SDK\Common\Time\ClockFactory;

use Nucleardog\Telemetry\Application;
use Nucleardog\Telemetry\Endpoint;

class OtlpHttpEndpoint extends Endpoint
{

	public function __construct(
		private string $url,
		private string $service,
	) {
		$this->url = rtrim($this->url, '/');
	}

	private function getTransport(string $type): TransportInterface
	{
		$factory = new OtlpHttpTransportFactory();
		return $factory->create(sprintf('%s/v1/%s', $this->url, $type), ContentTypes::JSON);
	}

	public function getLogger(Application $app): LoggerInterface
	{
		return $this->buildLoggerProvider(
			fn($builder) => $builder
				->setResource($app->build())
				->addLogRecordProcessor(new BatchLogRecordProcessor(new LogsExporter($this->getTransport('logs')), ClockFactory::getDefault()))
		)->getLogger($app->name);
	}

	public function getTracer(Application $app): TracerInterface
	{
		return $this->buildTracerProvider(
			fn($builder) => $builder
				->setResource($app->build())
				->addSpanProcessor(new SimpleSpanProcessor(new SpanExporter($this->getTransport('traces'))))
		)->getTracer($app->name);
	}

	public function getMetrics(Application $app): MeterInterface
	{
		return $this->buildMeterProvider(
			fn($builder) => $builder
				->setResource($app->build())
				->addReader(new ExportingReader(new MetricExporter($this->getTransport('metrics'))))
		)->getMeter($app->name);
	}

}