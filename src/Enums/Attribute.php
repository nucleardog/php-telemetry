<?php

namespace Nucleardog\Telemetry\Enums;

abstract class Attribute
{

	const CONTEXT_TYPE = 'CONTEXT_TYPE';

	const HTTP_HOST = 'HTTP_HOST';
	const HTTP_URL = 'HTTP_URL';
	const HTTP_METHOD = 'HTTP_METHOD';
	const HTTP_STATUS_CODE = 'HTTP_STATUS_CODE';
	const HTTP_ROUTE = 'HTTP_ROUTE';

	const ERROR_NAME = 'ERROR_NAME';
	const ERROR_MESSAGE = 'ERROR_MESSAGE';

	const PROCESS_EXIT_CODE = 'PROCESS_EXIT_CODE';
	const PROCESS_COMMAND = 'PROCESS_COMMAND';

}
