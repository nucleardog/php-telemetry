<?php

namespace Nucleardog\Telemetry\Enums;

enum Attributes : string
{

	case CONTEXT_TYPE = 'nd.tel/type';

	case HTTP_HOST = 'http.host';
	case HTTP_URL = 'http.url';
	case HTTP_METHOD = 'http.method';
	case HTTP_STATUS_CODE = 'http.status_code';
	case HTTP_ROUTE = 'http.route';

	case ERROR_NAME = 'error.name';
	case ERROR_MESSAGE = 'error.message';

	case PROCESS_EXIT_CODE = 'process.exit_code';
	case PROCESS_COMMAND = 'process.command';

}
