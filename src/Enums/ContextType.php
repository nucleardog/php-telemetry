<?php

namespace Nucleardog\Telemetry\Enums;

enum ContextType : string
{

	case JOB = 'job';
	case REQUEST = 'request';
	case PROCESS = 'process';

}
