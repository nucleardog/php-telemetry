<?php

declare(strict_types=1);

namespace Nucleardog\Telemetry\Laravel\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

use Nucleardog\Telemetry\Telemetry;
use Nucleardog\Telemetry\Traces\Context;

/**
 * Continue a trace if the appropriate trace and span id were passed in the
 * request headers.
 */
class ContinueTrace
{

	public function __construct(
		private Telemetry $telemetry
	) {
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response) $next
	 * @param Request                                                                          $request
	 * @param string                                                                           $header
	 */
	public function handle(Request $request, Closure $next, string $header = 'X-Trace'): Response
	{
		$trace = $request->header($header);

		if (!empty($trace)) {
			$this->telemetry->traces->setParent($trace);
		}

		return $next($request);
	}
}