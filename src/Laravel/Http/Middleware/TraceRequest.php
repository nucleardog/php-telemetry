<?php

declare(strict_types=1);

namespace Nucleardog\Telemetry\Laravel\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

use Nucleardog\Telemetry\Telemetry;
use Nucleardog\Telemetry\Attributes;
use Nucleardog\Telemetry\Enums\Attribute;
use Nucleardog\Telemetry\Enums\ContextType;

/**
 * Start a trace span for each request
 */
class TraceRequest
{

	public function __construct(
		private Telemetry $telemetry
	) {
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response) $next
	 * @param Request                                                                          $request
	 */
	public function handle(Request $request, Closure $next): Response
	{
		$context = $this->telemetry->traces->enter($request->path());

		Attributes::fill($context, [
			Attribute::CONTEXT_TYPE => ContextType::REQUEST,
			Attribute::HTTP_HOST => $request->host(),
			Attribute::HTTP_URL => $request->fullUrl(),
			Attribute::HTTP_METHOD => $request->method(),
		]);

		try
		{
			$result = $next($request);
			if (method_exists($result, 'header'))
			{
				if (count($this->telemetry->traces) > 0)
					$result->header('X-Trace', $this->telemetry->traces->encode());
			}
		}
		finally
		{
			Attributes::set($context, Attribute::HTTP_ROUTE, Attributes::when(!empty($request->route()), fn() => $request->route()->uri));
			$this->telemetry->traces->end();
			$this->telemetry->traces->clearParent();
		}

		Attributes::set($context, Attribute::HTTP_STATUS_CODE, Attributes::when(method_exists($result, 'getStatusCode'), fn() => $result->getStatusCode()));

		return $result;
	}
}