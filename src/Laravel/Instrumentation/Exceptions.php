<?php

declare(strict_types=1);

namespace Nucleardog\Telemetry\Laravel\Instrumentation;

use Nucleardog\Telemetry\Laravel\Support\InstrumentationServiceProvider;
use Nucleardog\Telemetry\Attributes;
use Nucleardog\Telemetry\Enums\Attribute;
use Illuminate\Contracts\Debug\ExceptionHandler;

class Exceptions extends InstrumentationServiceProvider
{
	public function getName(): string
	{
		return 'exceptions';
	}

	public function boot(): void
	{
		if (!$this->isEnabled()) return;

		$exceptionHandler = app()->make(ExceptionHandler::class);
		$exceptionHandler->reportable(function(\Throwable $ex) {
			$traces = app()->make(\Nucleardog\Telemetry\Telemetry::class)->traces;
			if (count($traces) > 0)
			{
				$context = $traces->current();
				$context->exception($ex);

				Attributes::fill($context, [
					Attribute::ERROR_NAME => class_basename(get_class($ex)),
					Attribute::ERROR_MESSAGE => $ex->getMessage(),
				]);

			}
		});

	}

}