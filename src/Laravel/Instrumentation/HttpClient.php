<?php

declare(strict_types=1);

namespace Nucleardog\Telemetry\Laravel\Instrumentation;

use Nucleardog\Telemetry\Laravel\Support\InstrumentationServiceProvider;
use Nucleardog\Telemetry\Attributes;
use Nucleardog\Telemetry\Enums\Attribute;
use Nucleardog\Telemetry\Enums\ContextType;
use Illuminate\Support\Facades\Http;

class HttpClient extends InstrumentationServiceProvider
{
	public function getName(): string
	{
		return 'http_client';
	}

	public function boot(): void
	{
		if (!$this->isEnabled()) return;

		Http::globalMiddleware(function($handler) {
			return function($request, $options) use ($handler) {

				$context = app()->make(\Nucleardog\Telemetry\Telemetry::class)->traces->enter(
					(string)$request->getUri(),
					Attributes::resolve([Attribute::CONTEXT_TYPE => ContextType::REQUEST]),
				);

				// Add the trace header to the outgoing request
				$request = $request->withHeader('X-Trace', $context->encode()['parent']);

				// Pass the request down the chain
				return $handler($request, $options)->then(
					// Request succeeded
					function($response) use ($context) {
						// Add some response data to the context
						Attributes::fill($context, [
							Attribute::HTTP_STATUS_CODE => $response->getStatusCode(),
						]);

						// Set success/fail based on standard HTTP status codes
						if ($response->getStatusCode() >= 400)
							$context->error();
						else
							$context->success();

						$context->leave();

						return $response;
					},
					function (\Throwable $ex) use ($context) {
						$context->exception($ex);
						$context->leave();
					},
				);

			};
		});

	}

}