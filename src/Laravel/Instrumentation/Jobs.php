<?php

declare(strict_types=1);

namespace Nucleardog\Telemetry\Laravel\Instrumentation;

use Nucleardog\Telemetry\Laravel\Support\InstrumentationServiceProvider;
use Nucleardog\Telemetry\Attributes;
use Nucleardog\Telemetry\Enums\Attribute;
use Nucleardog\Telemetry\Enums\ContextType;
use Illuminate\Support\Facades\Event;
use Illuminate\Queue\Events\JobPopped;
use Illuminate\Queue\Events\JobProcessed;
use Illuminate\Queue\Events\JobFailed;
use Illuminate\Queue\Events\JobExceptionOccurred;
use Illuminate\Queue\Events\JobReleasedAfterException;
use Illuminate\Queue\Events\JobTimedOut;

class Jobs extends InstrumentationServiceProvider
{
	public function getName(): string
	{
		return 'jobs';
	}

	public function boot(): void
	{
		if (!$this->isEnabled()) return;

		// Register a callback with the base queue class that allows us to inject the trace id
		// into job payloads.
		\Illuminate\Queue\Queue::createPayloadUsing(function(string $connection, string $queue, array $payload) {
			// See if a trace is open
			$traces = app()->make(\Nucleardog\Telemetry\Telemetry::class)->traces;
			if (count($traces))
			{
				return [
					'X-Trace' => $traces->current()->encode(),
				];
			}
			else
			{
				return [];
			}
		});

		// Register an event handler so we can continue the trace when a job begins
		// processing.
		Event::listen(function(JobPopped $event) {
			$payload = $event->job->payload();
			$traces = app()->make(\Nucleardog\Telemetry\Telemetry::class)->traces;

			// If there's a trace id in the job and no trace is currently in progress
			if (!empty($payload['X-Trace']) && count($traces) === 0)
			{
				$parent = $payload['X-Trace']['parent'] ?? '';
				$state = $payload['X-Trace']['state'] ?? null;
				$traces->setParent($parent, $state);
			}

			$context = $traces->enter(
				class_basename($payload['data']['commandName']),
				Attributes::resolve([
					Attribute::CONTEXT_TYPE => ContextType::JOB,
				]),
			);
			// TODO: Fill any extra context? Job variables?
		});

		Event::listen(function(JobProcessed $event) {
			$traces = app()->make(\Nucleardog\Telemetry\Telemetry::class)->traces;
			if (count($traces) > 0) {
				$traces->event('Job processed')->success()->end();
			}
		});

		Event::listen(function(JobFailed $event) {
			$traces = app()->make(\Nucleardog\Telemetry\Telemetry::class)->traces;
			if (count($traces) > 0) {
				$traces->event('Job failed');
			}
		});

		Event::listen(function(JobExceptionOccurred $event) {
			$traces = app()->make(\Nucleardog\Telemetry\Telemetry::class)->traces;
			if (count($traces) > 0) {
				$traces->exception($event->exception, 'Job failed')->end();
			}
		});

		Event::listen(function(JobReleasedAfterException $event) {
			// Already left span during jobexception
		});

		Event::listen(function(JobTimedOut $event) {
			$traces = app()->make(\Nucleardog\Telemetry\Telemetry::class)->traces;
			if (count($traces) > 0) {
				$traces->end();
			}
		});

	}

}