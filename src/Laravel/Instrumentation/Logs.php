<?php

declare(strict_types=1);

namespace Nucleardog\Telemetry\Laravel\Instrumentation;

use Nucleardog\Telemetry\Laravel\Support\InstrumentationServiceProvider;
use Illuminate\Support\Facades\Event;
use Illuminate\Log\Events\MessageLogged;

class Logs extends InstrumentationServiceProvider
{
	public function getName(): string
	{
		return 'logs';
	}

	public function boot(): void
	{
		if (!$this->isEnabled()) return;

		Event::listen(function(MessageLogged $event) {
			$logs = app()->make(\Nucleardog\Telemetry\Telemetry::class)->logs;
			$level = $event->level;
			$logs->$level($event->message, $event->context);
		});

	}

}