<?php

declare(strict_types=1);

namespace Nucleardog\Telemetry\Laravel\Instrumentation\Process;
use Illuminate\Process\Factory as LaravelFactory;

class Factory extends LaravelFactory
{
	public function newPendingProcess()
	{
		return (new PendingProcess($this))->withFakeHandlers($this->fakeHandlers);
	}
}