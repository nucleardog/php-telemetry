<?php

declare(strict_types=1);

namespace Nucleardog\Telemetry\Laravel\Instrumentation\Process;
use Illuminate\Process\PendingProcess as LaravelPendingProcess;
use Nucleardog\Telemetry\Telemetry;
use Nucleardog\Telemetry\Attributes;
use Nucleardog\Telemetry\Enums\Attribute;
use Nucleardog\Telemetry\Enums\ContextType;

class PendingProcess extends LaravelPendingProcess
{

    public function run(array|string $command = null, callable $output = null)
	{
		$traces = app()->make(Telemetry::class)->traces;

		$command = $command ?: $this->command;

		return $traces->in(
			is_array($command) ? implode(' ', $command) : $command,
			function($context) use ($command, $output) {
 		   		$result = parent::run($command, $output);

				Attributes::fill($context, [
					Attribute::PROCESS_EXIT_CODE => $result->exitCode(),
					Attribute::PROCESS_COMMAND => $result->command(),
				]);

				if ($result->successful())
					$context->success();
				else
					$context->error();

				return $result;
			},
			Attributes::resolve([
				Attribute::CONTEXT_TYPE => ContextType::PROCESS,
			]),
		);
	}

}