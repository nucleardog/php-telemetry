<?php

declare(strict_types=1);

namespace Nucleardog\Telemetry\Laravel\Instrumentation;

use Nucleardog\Telemetry\Laravel\Support\InstrumentationServiceProvider;

use Illuminate\Process\Factory as LaravelFactory;
use Nucleardog\Telemetry\Laravel\Instrumentation\Process\Factory as TelemetryFactory;


class Processes extends InstrumentationServiceProvider
{
	public function getName(): string
	{
		return 'processes';
	}

	protected function getConfigurationSchema(): array
	{
		return [];
	}

	public function boot(): void
	{
		if (!$this->isEnabled()) return;

		$this->app->bind(LaravelFactory::class, TelemetryFactory::class);
	}

}