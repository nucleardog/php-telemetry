<?php

declare(strict_types=1);

namespace Nucleardog\Telemetry\Laravel\Instrumentation;

use Nucleardog\Telemetry\Laravel\Support\InstrumentationServiceProvider;

use Illuminate\Contracts\Http\Kernel;


class Requests extends InstrumentationServiceProvider
{
	public function getName(): string
	{
		return 'requests';
	}

	protected function getConfigurationSchema(): array
	{
		return [
			'continue_traces' => ['required', 'boolean'],
		];
	}

	protected function getDefaultConfiguration(): array
	{
		return [
			'continue_traces' => true,
		];
	}

	public function boot(): void
	{
		// Add aliases for our middleware
		$this->app['router']->aliasMiddleware('continue-trace', \Nucleardog\Telemetry\Laravel\Http\Middleware\ContinueTrace::class);
		$this->app['router']->aliasMiddleware('trace', \Nucleardog\Telemetry\Laravel\Http\Middleware\TraceRequest::class);

		if (!$this->isEnabled()) return;

		// Automatically add the request tracing to the global middleware
		$kernel = app()->make(Kernel::class);
		$kernel->prependMiddleware(\Nucleardog\Telemetry\Laravel\Http\Middleware\TraceRequest::class);
		// If enabled, add the middleware to continue traces based on HTTP headers
		if ($this->config('continue_traces'))
			$kernel->prependMiddleware(\Nucleardog\Telemetry\Laravel\Http\Middleware\ContinueTrace::class);
	}

}