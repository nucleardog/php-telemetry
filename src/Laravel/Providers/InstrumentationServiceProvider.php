<?php

namespace Nucleardog\Telemetry\Laravel\Providers;

use Illuminate\Support\AggregateServiceProvider;
use Illuminate\Foundation\Console\AboutCommand;

class InstrumentationServiceProvider extends AggregateServiceProvider
{
	protected $providers = [
		\Nucleardog\Telemetry\Laravel\Instrumentation\Requests::class,
		\Nucleardog\Telemetry\Laravel\Instrumentation\Jobs::class,
		\Nucleardog\Telemetry\Laravel\Instrumentation\Exceptions::class,
		\Nucleardog\Telemetry\Laravel\Instrumentation\Logs::class,
		\Nucleardog\Telemetry\Laravel\Instrumentation\Processes::class,
		\Nucleardog\Telemetry\Laravel\Instrumentation\HttpClient::class,
	];

	public function boot(): void
	{
		AboutCommand::add('Nucleardog\Telemetry', fn() => $this->about());
	}

	private function about(): array
	{
		$about = [];

		foreach ($this->instances as $provider)
		{
			$about[$provider->getName()] = $provider->isEnabled() ? '<fg=green>active</>' : '<fg=gray>inactive</>';
		}

		return $about;
	}

}