<?php

namespace Nucleardog\Telemetry\Laravel\Providers;

use Illuminate\Support\AggregateServiceProvider;
use Illuminate\Foundation\Console\AboutCommand;
use Illuminate\Contracts\Foundation\Application;

class TelemetryServiceProvider extends AggregateServiceProvider
{
	protected $providers = [
		InstrumentationServiceProvider::class,
	];

	public function register(): void
	{
		parent::register();

		// Load our config
		$this->mergeConfigFrom($this->getConfigPath(), 'telemetry');

		$this->app->singleton(\Nucleardog\Telemetry\Application::class, function(Application $app) {
			return new \Nucleardog\Telemetry\Application(...$app['config']->get('telemetry.app'));
		});

		$this->app->singleton(\Nucleardog\Telemetry\Telemetry::class, function(Application $app) {
			$builder = new \Nucleardog\Telemetry\TelemetryBuilder();
			$builder->setApplication($appInfo = $app->make(\Nucleardog\Telemetry\Application::class));

			match($app['config']->get('telemetry.endpoint.type')) {
				'null' => $builder->useNullEndpoint(),
				'console' => $builder->useConsole(),
				'otlp-http' => $builder->useOtlpHttp($app['config']->get('telemetry.endpoint.url'), $appInfo->name),
				'otlp-grpc' => $builder->useOtlpGrpc($app['config']->get('telemetry.endpoint.url'), $appInfo->name),
			};

			return $builder->build();
		});

		$this->app->bind(\Nucleardog\Telemetry\Traces\Traces::class, function(Application $app) {
			return app()->make(\Nucleardog\Telemetry\Telemetry::class)->traces;
		});
	}

	public function boot(): void
	{
		// Publish our config in case the user wants to modify it
		$this->publishes([
			$this->getConfigPath() => config_path('telemetry.php'),
		]);
	}

	private function getConfigPath(): string
	{
		return __DIR__.implode(DIRECTORY_SEPARATOR, ['', '..', '..', '..', 'config', 'telemetry.php']);
	}

}