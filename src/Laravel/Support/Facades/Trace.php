<?php

declare(strict_types=1);

namespace Nucleardog\Telemetry\Laravel\Support\Facades;
use Illuminate\Support\Facades\Facade;
use Nucleardog\Telemetry\Traces\Context;

/**
 * @method static Trace setParent(string $parent, ?string $state = null)
 * @method static Trace clearParent()
 * @method static void end()
 * @method static Context current()
 * @method static int count()
 * @method static Context enter(string $name, array<string,mixed>|null $attributes = null)
 * @method static mixed in(string $name, \Closure $callback, array<string,mixed>|null $attributes = null)
 * @method static Trace leave()
 * @method static array encode()
 * @method static Context event(string $name, array<string,mixed>|null $attributes = null)
 * @method static Context exception(\Throwable $ex, ?string $message = null)
 * @method static Context error(?string $message = null)
 * @method static Context success()
 * @method static float elapsedSeconds()
 * @method static int elapsedMs()
 * @method static int elapsedNs()
 * @method static Context attributes(array<string,mixed> $attributes)
 */
class Trace extends Facade
{
	protected static function getFacadeAccessor() { return \Nucleardog\Telemetry\Traces\Traces::class; }
}
