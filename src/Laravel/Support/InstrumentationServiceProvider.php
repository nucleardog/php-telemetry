<?php

namespace Nucleardog\Telemetry\Laravel\Support;

use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\Console\AboutCommand;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

abstract class InstrumentationServiceProvider extends ServiceProvider
{
	private array $config;

	public abstract function getName(): string;

	public function register(): void
	{
	}

	public function boot(): void
	{
	}

	protected function config(string $key, mixed $default = null): mixed
	{
		if (!isset($this->config))
		{
			$this->config = $this->resolveConfiguration();
		}
		return Arr::get($this->config, $key, $default);
	}

	public function isEnabled(): bool
	{
		return filter_var(
			$this->app['config']->get(sprintf('telemetry.instrument.%s', $this->getName())),
			\FILTER_VALIDATE_BOOLEAN,
		);
	}

	protected function getDefaultConfiguration(): array
	{
		return [];
	}

	protected function getConfigurationSchema(): array
	{
		return [];
	}

	protected function resolveConfiguration(): array
	{
		$config = Arr::undot(
			array_merge(
				Arr::dot($this->getDefaultConfiguration()),
				Arr::dot($this->app['config']->get(sprintf('telemetry.%s', $this->getName())) ?? []),
			)
		);

		$validator = Validator::make($config, $this->getConfigurationSchema());

		try
		{
			$validator->validate();
			return $validator->validated();
		}
		catch (ValidationException $ex)
		{
			throw new \DomainException(sprintf('Invalid configuration for instrumentation provider: %s', $this->getName()), previous: $ex);
		}
	}

}
