<?php

declare(strict_types=1);

namespace Nucleardog\Telemetry\Logs;
use Psr\Log\LoggerInterface;
use Psr\Log\LoggerTrait;
use Psr\Log\LogLevel;

use OpenTelemetry\API\Logs\LoggerInterface as OtelLoggerInterface;
use OpenTelemetry\API\Logs\LogRecord;
use OpenTelemetry\API\Logs\Map\Psr3;

class Logs implements LoggerInterface
{
	use LoggerTrait;

	public function __construct(
		private OtelLoggerInterface $logger,
	) {
	}

	public function log($level, string|\Stringable $message, array $context = []): void
	{
		// Create the log record
		$record = new LogRecord($message);
		$record->setSeverityText($level);
		$record->setSeverityNumber(Psr3::severityNumber($level));
		$record->setAttributes($context);
		$this->logger->emit($record);
	}

}