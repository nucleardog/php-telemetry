<?php

declare(strict_types=1);

namespace Nucleardog\Telemetry;

use OpenTelemetry\Context\ContextInterface;
use OpenTelemetry\Context\Context;
use OpenTelemetry\API\Trace\Span;

class Telemetry
{
	private Traces\Traces $traces;
	private Logs\Logs $logs;

	public function __construct(
		private Application $app,
		private Endpoint $logEndpoint,
		private Endpoint $metricEndpoint,
		private Endpoint $traceEndpoint,
		private ?ContextInterface $parentContext,

	) {
		register_shutdown_function(function() {
			$this->shutdown();
		});
	}

	public function traces(): Traces\Traces
	{
		if (!isset($this->traces)) {
			$this->traces = new Traces\Traces(
				tracer: $this->traceEndpoint->getTracer($this->app),
				logger: $this->logEndpoint->getLogger($this->app),
				parentContext: $this->parentContext,
			);
		}

		return $this->traces;
	}

	public function logs(): Logs\Logs
	{
		if (!isset($this->logs)) {
			$this->logs = new Logs\Logs(
				logger: $this->logEndpoint->getLogger($this->app),
			);
		}

		return $this->logs;
	}

	public function __get(string $key): mixed
	{
		return match($key) {
			'traces' => $this->traces(),
			'logs' => $this->logs(),
		};
	}

	public function shutdown(): void
	{
		// Clean up all open scopes/spans
		while ($scope = Context::storage()->scope())
		{
			$scope->detach();
			$span = Span::fromContext($scope->context());
			$span->end();
		}
		// Trigger shutdown on any providers created by the endpoints
		$this->logEndpoint->shutdown();
		$this->traceEndpoint->shutdown();
		$this->metricEndpoint->shutdown();
	}

}