<?php

declare(strict_types=1);

namespace Nucleardog\Telemetry;

use OpenTelemetry\Context\ContextInterface;

class TelemetryBuilder
{
	protected Application $app;

	protected Endpoint $logEndpoint;
	protected Endpoint $metricEndpoint;
	protected Endpoint $traceEndpoint;

	protected ?ContextInterface $parentContext = null;
	// Link attributes?

	public function setApplication(Application $app): static
	{
		$this->app = $app;
		return $this;
	}

	public function setEndpoint(Endpoint $endpoint): static
	{
		$this->logEndpoint = $endpoint;
		$this->metricEndpoint = $endpoint;
		$this->traceEndpoint = $endpoint;
		return $this;
	}

	public function setLogEndpoint(Endpoint $endpoint): static
	{
		$this->logEndpoint = $endpoint;
		return $this;
	}

	public function setMetricEndpoint(Endpoint $endpoint): static
	{
		$this->metricEndpoint = $endpoint;
		return $this;
	}

	public function setTraceEndpoint(Endpoint $endpoint): static
	{
		$this->traceEndpoint = $endpoint;
		return $this;
	}

	public function useConsole(): static
	{
		$this->setEndpoint(new Endpoints\ConsoleEndpoint());
		return $this;
	}

	public function useNullEndpoint(): static
	{
		$this->setEndpoint(new Endpoints\NullEndpoint());
		return $this;
	}

	public function useOtlpGrpc(string $url, string $service): static
	{
		$this->setEndpoint(new Endpoints\OtlpGrpcEndpoint(url: $url, service: $service));
		return $this;
	}

	public function useOtlpHttp(string $url, string $service): static
	{
		$this->setEndpoint(new Endpoints\OtlpHttpEndpoint(url: $url, service: $service));
		return $this;
	}

	public function setParent(string $parent, ?string $state = null): static
	{
		$this->parentContext = Traces\Context::decode($parent, $state);
		return $this;
	}

	public function build(): Telemetry
	{
		$props = get_object_vars($this);
		return new Telemetry(...$props);
	}
}