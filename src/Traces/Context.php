<?php

declare(strict_types=1);

namespace Nucleardog\Telemetry\Traces;

use OpenTelemetry\API\Trace\Span;
use OpenTelemetry\API\Trace\Propagation\TraceContextPropagator;
use OpenTelemetry\Context\ContextInterface;
use OpenTelemetry\SDK\Common\Attribute\Attributes;
use OpenTelemetry\API\Trace\StatusCode;
use OpenTelemetry\Context\ScopeInterface;

class Context implements \ArrayAccess
{

	public function __construct(
		private Traces $traces,
		private Span $span,
		private ScopeInterface $scope,
	) {
	}

	public function getSpan(): Span
	{
		return $this->span;
	}

	public function getScope(): ScopeInterface
	{
		return $this->scope;
	}

	public function encode(): array
	{
		// The trace propagator will only work on Context, but in the end
		// really only needs SpanContext. Code written referenced
		// from TraceContextPropagator::inject.
		$spanContext = $this->span->getContext();
		return [
			'parent' => sprintf(
				'00-%s-%s-%s',
				$spanContext->getTraceId(),
				$spanContext->getSpanId(),
				$spanContext->isSampled() ? '01' : '00',
			),
			'state' => $spanContext->getTraceState() !== '' ? $spanContext->getTraceState() : null,
		];

	}

	public static function decode(string $parent, ?string $state = null): ContextInterface
	{
		// TODO: Pretty sure this always just overwrites the current context.
		return TraceContextPropagator::getInstance()->extract([
			'traceparent' => $parent,
			'tracestate' => $state,
		]);
	}

	public function leave(): ?static
	{
		$this->traces->leave();
		return count($this->traces) !== 0 ? $this->traces->current() : null;
	}

	public function end(): Traces
	{
		$this->traces->end();
		return $this->traces;
	}

	public function event(string $name, ?array $attributes = []): static
	{
		if (empty($attributes))
			$this->span->addEvent($name);
		else
			$this->span->addEvent($name, Attributes::create($attributes));
		return $this;
	}

	public function exception(\Throwable $ex, ?string $message = null): static
	{
		$this->span->setStatus(StatusCODE::STATUS_ERROR, $message ?? $ex->getMessage());
		$this->span->recordException($ex);
        // ['exception.escaped' => true] should be passed as the second argument if we know
        // this exception will result in us leaving this span
        return $this;
	}

	public function error(?string $message = null): static
	{
		$this->span->setStatus(StatusCode::STATUS_ERROR, $message);
		return $this;
	}

	public function success(): static
	{
		// TODO: Once STATUS_OK is set, any further setStatus calls are a no-op.
		//       There's no easy way to retrieve the current status from the span.
		//       Either track it locally or... idk.
		$this->span->setStatus(StatusCode::STATUS_OK);
		return $this;
	}

	public function elapsedSeconds(): float
	{
		return $this->elapsedMs() / 1000;
	}

	public function elapsedMs(): int
	{
		return (int)floor($this->elapsedNs() / 1000000);
	}

	public function elapsedNs(): int
	{
		return $this->span->getDuration();
	}

	public function attributes(array $attributes): static
	{
		$this->span->setAttributes($attributes);
		return $this;
	}

	public function offsetGet(mixed $offset): mixed
	{
		return $this->span->getAttribute((string)$offset);
	}

	public function offsetSet(mixed $offset, mixed $value): void
	{
		$this->span->setAttribute((string)$offset, $value);
	}

	public function offsetExists(mixed $offset): bool
	{
		return !empty($this->span->getAttribute((string)$offset));
	}

	public function offsetUnset(mixed $offset): void
	{
		$this->span->setAttribute((string)$offset, null);
	}

}