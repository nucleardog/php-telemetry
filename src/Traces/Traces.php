<?php

declare(strict_types=1);

namespace Nucleardog\Telemetry\Traces;

use OpenTelemetry\API\Trace\TracerInterface;
use OpenTelemetry\API\Logs\LoggerInterface;

use OpenTelemetry\Context\ScopeInterface;

use OpenTelemetry\API\Trace\TraceFlags;
use OpenTelemetry\API\Trace\SpanContext;
use OpenTelemetry\API\Trace\SpanKind;
use OpenTelemetry\API\Trace\Span;

use OpenTelemetry\Context\ContextInterface;


/**
 * @property Traces\Traces $traces
 */
class Traces implements \Countable
{
	private array $contexts;

	public function __construct(
		private TracerInterface $tracer,
		private LoggerInterface $logger,
		private ?ContextInterface $parentContext = null,
	) {
		$this->contexts = [];
	}

	public function setParent(string $parent, ?string $state = null): static
	{
		$context = Context::decode($parent, $state);
		return $this->setParentContext($context);
	}

	public function clearParent(): static
	{
		if (sizeof($this->contexts) > 0)
			throw new \RuntimeException('Cannot set parent while in a context.');

		$this->parentContext = null;

		return $this;
	}

	public function setParentContext(ContextInterface $context): static
	{
		if (sizeof($this->contexts) > 0)
			throw new \RuntimeException('Cannot set parent while in a context.');

		$this->parentContext = $context;
		return $this;
	}

	public function end(): void
	{
		while (sizeof($this->contexts))
			$this->leave();
	}

	public function root(): Context
	{
		if (empty($this->contexts))
			throw new \RuntimeException('No active trace contexts.');

		return $this->contexts[0];
	}

	public function current(): Context
	{
		if (empty($this->contexts))
			throw new \RuntimeException('No active trace contexts.');

		return end($this->contexts);
	}

	public function count(): int
	{
		return sizeof($this->contexts);
	}

	public function enter(string $name, ?array $attributes = null): Context
	{
		// Start building the span
		$builder = $this->tracer->spanBuilder($name);
		// If no spans have been entered, create a root span
		$is_root_span = sizeof($this->contexts) === 0;

		if ($is_root_span)
		{
			// Create a root span

			// If a parent span was provided, set it as our parent
			if (isset($this->parentContext))
			{
				$builder->setParent($this->parentContext);
			}

			// Use SERVER since we're a server processing a request
			$builder->setSpanKind(SpanKind::KIND_SERVER);
		}
		else
		{
			// Create a nested span

			// Set this to internal since this is us nesting spans within the
			// same invocation of the application.
			$builder->setSpanKind(SpanKind::KIND_INTERNAL);
		}

		// Build the span and context
		$span = $builder->startSpan();
		$scope = $span->activate();
		$this->contexts[] = $context = new Context(
			traces: $this,
			span: $span,
			scope: $scope,
		);

		if (!empty($attributes))
		{
			$span->setAttributes($attributes);
		}

		return $context;
	}

	public function in(string $name, \Closure $callback, ?array $attributes = null): mixed
	{
		$context = $this->enter($name, $attributes);
		try
		{
			$return = $callback($context);
		}
		catch (\Throwable $ex)
		{
			$this->exception($ex);
			throw $ex;
		}
		finally
		{
			$this->leave();
		}
		return $return;
	}

	public function leave(): static
	{
		if (empty($this->contexts))
			throw new \RuntimeException('No active trace contexts.');

		// Remove the last context from the stack
		$context = array_pop($this->contexts);

		// End the span
		$span = $context->getSpan();
		$scope = $context->getScope();
		$span->end();
		$scope->detach();

		return $this;
	}

	public function __call(string $method, array $args): mixed
	{
		$context = $this->current();
		return call_user_func_array([$context, $method], $args);
	}

}